import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { HelperService } from './helper-service';

@Injectable()
export class OrdersService {
  constructor(public http: Http,
              private helper: HelperService) {
    console.log('Hello OrdersService Provider');
  }

  parseStatus(status_id){
    let color = '';
    switch(parseInt(status_id)){
      case 1:
          color = '#ffaa00';
        break;
      case 2:
          color = '#3ddcf7';
        break;
      case 3:
          color = '#00b19d';
        break;
      case 5:
          color = '#f76397';
        break;
      case 7:
          color = '#ef5350';
        break;
      default:
        color = '#283e4a';
    }
    return color;
  }

  getOrders(){
    return new Promise(resolve => {
      this.http.get(this.helper.API.HOST+'/user/'+this.helper.ME.uuid+'/order',
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }

  getOrder(order_uuid){
    return new Promise(resolve => {
      this.http.get(this.helper.API.HOST+'/user/'+this.helper.ME.uuid+'/order/'+order_uuid,
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }

  getItems(order_uuid){
    return new Promise(resolve => {
      this.http.get(this.helper.API.HOST+'/user/'+this.helper.ME.uuid+'/order/'+order_uuid+'/items',
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }

  getStatuses(order_uuid){
    return new Promise(resolve => {
      this.http.get(this.helper.API.HOST+'/user/'+this.helper.ME.uuid+'/order/'+order_uuid+'/statuses',
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }


}
