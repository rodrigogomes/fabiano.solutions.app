import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { HelperService } from './helper-service';

@Injectable()
export class CoursesService {

  constructor(public http: Http,
              private helper: HelperService) {
    console.log('Hello CoursesService Provider');
  }

  getCourses(){
    return new Promise(resolve => {
      this.http.get(this.helper.API.HOST+'/course',
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }

}
