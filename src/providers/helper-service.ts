import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class HelperService {
  API = {
    // HOST: 'http://localhost:3000/api/v1',
    HOST:  '',
    TOKEN: 'YOUR_TOKEN_API_TOKEN',
    HEADERS: null
  }
  ME: {
    uuid: null
  };
  logged: boolean = false;
  headers: any;
  constructor(public http: Http,
              private events: Events) {
    console.log('Hello HelperService Provider');
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Token token='+this.API.TOKEN);
    this.getHeaders();
  }

  updateMe(me){
    this.ME = me;
    this.events.publish('me:observer', me);
    this.ME != null ? this.logged = true : this.logged = false;
  }

  getHeaders(){
    return this.API.HEADERS = this.headers;
  }

  mainFormatTimeStamp(timestamp){
    try{
      let current_ts = timestamp.split('T');
      let current_date = current_ts[0].split('-');
      current_date = current_date[2]+'-'+current_date[1]+'-'+current_date[0];

      let current_time = current_ts[1].split('.');
      current_time = current_time[0].split(':');
      current_time = current_time[0]+':'+current_time[1];

      let current_return = new Array(current_date, current_time);
      return current_return;
    }catch(e){
      return timestamp;
    }
  }

  formatTimeStamp(timestamp, return_type){
    try{
      let current_ts = this.mainFormatTimeStamp(timestamp);
      if(return_type == 'date'){
        return current_ts[0];
      }else if(return_type == 'time'){
        return current_ts[1];
      }else{
        return current_ts[0]+ ' às ' +current_ts[1];
      }
    }catch(e){
      return timestamp;
    }
  }

}
