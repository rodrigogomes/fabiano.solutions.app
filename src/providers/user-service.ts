import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { HelperService } from './helper-service';

@Injectable()
export class UserService {

  constructor(public http: Http,
              private helper: HelperService) {
    console.log('Hello UserService Provider');
  }

  signIn(email, password){
    let user = {
      email: email,
      password: password
    }
    return new Promise(resolve => {
      this.http.post(this.helper.API.HOST+'/user/login',
        JSON.stringify(user),
        {headers: this.helper.API.HEADERS})
        .map(res => res.json())
        .subscribe(
          data => {
                    if(data){
                      resolve(data);
                    }
                  },
          err  => {
                      resolve(false);
                  }
        );
    });
  }

}
