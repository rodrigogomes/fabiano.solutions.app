import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Orders } from '../pages/orders/orders';
import { Order } from '../pages/order/order';
import { Access } from '../pages/access/access';
import { Course } from '../pages/course/course';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { UserService } from '../providers/user-service';
import { CoursesService } from '../providers/courses-service';
import { OrdersService } from '../providers/orders-service';
import { HelperService } from '../providers/helper-service';

@NgModule({
  declarations: [
    MyApp,
    Access,
    AboutPage,
    HomePage,
    TabsPage,
    Orders,
    Order,
    Course
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Access,
    AboutPage,
    HomePage,
    TabsPage,
    Orders,
    Order,
    Course
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UserService,
    HelperService,
    CoursesService,
    OrdersService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
