import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

// services
import { HelperService } from '../../providers/helper-service';
import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-access',
  templateUrl: 'access.html',
})
export class Access {
  me: any = null;
  email: any = '';
  password: any = '';
  warning: any = null;
  constructor(public navCtrl: NavController,
              public helper: HelperService,
              private _user: UserService,
              private events: Events,
              public navParams: NavParams) {
              this.me = this.helper.ME;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Access');
    // listener current user change data
    this.events.subscribe('me:observer',(data)=>{
        this.me = data;
    });
  }

  // call sign in from api
  signIn(){
    this._user.signIn(this.email, this.password).then((data: any)=>{
      if(data){
        this.helper.updateMe(data.user);
        setTimeout(()=>{
          this.navCtrl.pop();
        },1000);
      }else{
        this.warning = 'Usuário ou senha inválidos';
      }
    });
  }

}
