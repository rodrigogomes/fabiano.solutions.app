import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// services
import { HelperService } from '../../providers/helper-service';
import { OrdersService } from '../../providers/orders-service';

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class Order {
  order: any = {};
  items: any = [];
  statuses: any = [];
  constructor(public navCtrl: NavController,
              public helper: HelperService,
              public _orders: OrdersService,
              public navParams: NavParams) {
              this.order = navParams.get('order');
              this.getOrder();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Order');
  }

  // get updated order data from api
  getOrder(){
    this._orders.getOrder(this.order.uuid).then((data: any)=>{
      this.order = data.order;
    });
  }

  // get order items from api
  getItems(){
    this._orders.getItems(this.order.uuid).then((data: any)=>{
      this.items = data.items;
    });
  }

  // get order statuses from api
  getStatuses(){
    this._orders.getStatuses(this.order.uuid).then((data: any)=>{
      this.statuses = data.statuses;
    });
  }

}
