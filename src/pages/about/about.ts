import { Component } from '@angular/core';
import { AlertController, ModalController, Events } from 'ionic-angular';
import { NavController } from 'ionic-angular';
// pages
import { Access } from '../access/access';
// services
import { HelperService } from '../../providers/helper-service';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  me: any = null;
  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              private events: Events,
              public modalCtrl: ModalController,
              public helper: HelperService) {
              this.me = this.helper.ME;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Orders');
    // listener current user change data
    this.events.subscribe('me:observer',(data)=>{
        this.me = data;
    });
  }

  // open acesse page for user sigin
  openAccess(){
    let modal = this.modalCtrl.create(Access, null, {showBackdrop: true});
    modal.present();
  }


  // confirm get out
  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Sair',
      message: 'Confirma deixar o app?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.closeAccess();
          }
        }
      ]
    });
    confirm.present();
  }

  // cleanning current user
  closeAccess(){
    this.helper.updateMe(null);
  }

}
