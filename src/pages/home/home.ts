import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// pages
import { Course } from '../course/course';
// services
import { CoursesService } from '../../providers/courses-service';
import { HelperService } from '../../providers/helper-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  courses: any = [];
  constructor(public navCtrl: NavController,
              public helper: HelperService,
              private _CoursesService: CoursesService) {
      this.getCourses();
  }

  // do refresh from swipe home page event
  doRefresh(refresher) {
    this.getCourses();
    setTimeout(() => {
     refresher.complete();
    }, 2000);
  }

  // get courses from api
  getCourses(){
    this._CoursesService.getCourses().then((data: any)=>{
      this.courses = data.courses;
    });
  }

  // open course page view
  openCourse(course){
    this.navCtrl.push(Course, {course: course});
  }

}
