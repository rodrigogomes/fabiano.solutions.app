import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-course',
  templateUrl: 'course.html',
})
export class Course {
  course: any = {};
  show_topics: boolean = false;
  show_skill: boolean = false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
              // get course object from page data transfer  
              this.course = navParams.get('course');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Course');
  }

}
