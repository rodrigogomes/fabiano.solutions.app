import { Component } from '@angular/core';
import { ModalController, Events } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
// pages
import { Access } from '../access/access';
import { Order } from '../order/order';
// services
import { HelperService } from '../../providers/helper-service';
import { OrdersService } from '../../providers/orders-service';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class Orders {
  orders: any = [];
  me: any = null;
  constructor(public navCtrl: NavController,
              public helper: HelperService,
              public _orders: OrdersService,
              private events: Events,
              public modalCtrl: ModalController,
              public navParams: NavParams) {
              this.me = this.helper.ME;
              this.getOrders();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Orders');
    // listener current user data change
    this.events.subscribe('me:observer',(data)=>{
        this.me = data;
        this.getOrders();
    });
  }

  // refresh from swipe orders event
  doRefresh(refresher) {
    this.getOrders();
    setTimeout(() => {
     refresher.complete();
    }, 2000);
  }

  // get orders from api
  getOrders(){
    if(!this.me){
      return;
    }
    this._orders.getOrders().then((data: any)=>{
      this.orders = data.orders;
    });
  }

  // open order page view
  openOrder(order){
    this.navCtrl.push(Order, {order: order});
  }

  // open access page for user sigin
  openAccess(){
    let modal = this.modalCtrl.create(Access, null, {showBackdrop: true});
    modal.present();
  }

}
